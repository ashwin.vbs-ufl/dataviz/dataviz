#include <string>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <iostream>
#include <netdb.h>
#include <unistd.h>
#include <cstdlib>

int sd;
addrinfo* res;
int x, y, z;
void randomStep(){
	int step = (((float)std::rand()/RAND_MAX) > 0.5) ? 1 : -1;
	float rand = ((float)std::rand()*100)/RAND_MAX;
	if(rand < 2) // move z
	{
		if(z == 0)
			z++;
		else if (z==9)
			z--;
		else
			z = z+step;
	}
	else if (rand < 30) //move y
	{
		if(y == 0)
			y++;
		else if (y==9)
			y--;
		else
			y = y+step;
	}
	else //move x
	{
		if(x == 0)
			x++;
		else if (x==9)
			x--;
		else
			x = x+step;
	}
}

void initConnection(){
	addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;  // use IPv4 or IPv6, whichever
	hints.ai_socktype = SOCK_DGRAM;
//	hints.ai_flags = AI_PASSIVE;
	getaddrinfo("localhost", "8089", &hints, &res);
	sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
}

std::string getDataPtString(int a, int b, int c){
	std::string comm("newDataPoint(name"), comma(", "), end(")"), sa, sb, sc;
	sa = std::to_string(a);
	sb = std::to_string(b);
	sc = std::to_string(c);
	return (comm + sa + sb + sc + comma + sa + comma + sb + comma + sc + end);
}

std::string getDataIncString(int a, int b, int c){
	std::string comm("incDataSize(name"), comma(", "), end(")"), sa, sb, sc;
	sa = std::to_string(a);
	sb = std::to_string(b);
	sc = std::to_string(c);
	return (comm + sa + sb + sc + end);
}

void Send(std::string msg){
	std::cout<<msg<<std::endl;
	sendto(sd, msg.c_str(), msg.length(), 0, res->ai_addr, res->ai_addrlen);
}

int main(){
	initConnection();

	std::string type("setType(pointmap)");
	Send(type);
	std::string scale("setScale(0.3)");
	Send(scale);

	for(int i = 0; i < 10; i++)
		for(int j = 0; j < 10; j++)
			for(int k = 0; k < 10; k++){
				Send(getDataPtString(i, j, k));
			}

	x = y = z = 5;

	for(int i = 0; i < 100000; i++){

		usleep(50000);

		randomStep();
		Send(getDataIncString(x, y, z));
	}

}
