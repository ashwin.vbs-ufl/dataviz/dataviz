#include "stglew.h"

#include <stdio.h>
#include <string.h>
#include <map>

#include "scene/DrawGraph.h"

	STVector3 mPosition;
	STVector3 mLookAt;
	STVector3 mRight;
	STVector3 mUp;

	void SetUpAndRight()
	{
		mRight = STVector3::Cross(mLookAt - mPosition, mUp);
		mRight.Normalize();
		mUp = STVector3::Cross(mRight, mLookAt - mPosition);
		mUp.Normalize();
	}

	void resetCamera()
	{
		mLookAt=STVector3(0.f,0.f,0.f);
		mPosition=STVector3(15.0f,0.0f,0.0f);
		mUp=STVector3(0.f,1.f,0.f);

		SetUpAndRight();
	}

	void resetUp()
	{
		mUp = STVector3(0.f,1.f,0.f);
		mRight = STVector3::Cross(mLookAt - mPosition, mUp);
		mRight.Normalize();
		mUp = STVector3::Cross(mRight, mLookAt - mPosition);
		mUp.Normalize();
	}
	/**
	 * Camera adjustment methods
	 */
	void RotateCamera(float delta_x, float delta_y)
	{
		float yaw_rate=1.f;
		float pitch_rate=1.f;

		mPosition -= mLookAt;
		STMatrix4 m;
		m.EncodeR(-1*delta_x*yaw_rate, mUp);
		mPosition = m * mPosition;
		m.EncodeR(-1*delta_y*pitch_rate, mRight);
		mPosition = m * mPosition;

		mPosition += mLookAt;
	}

	void ZoomCamera(float delta_y)
	{
		STVector3 direction = mLookAt - mPosition;
		float magnitude = direction.Length();
		direction.Normalize();
		float zoom_rate = 0.1f*magnitude < 0.5f ? .1f*magnitude : .5f;
		if(delta_y * zoom_rate + magnitude > 0)
		{
		mPosition += (delta_y * zoom_rate) * direction;
		}
	}

	void StrafeCamera(float delta_x, float delta_y)
	{
		float strafe_rate = 0.05f;

		mPosition -= strafe_rate * delta_x * mRight;
		mLookAt   -= strafe_rate * delta_x * mRight;
		mPosition += strafe_rate * delta_y * mUp;
		mLookAt   += strafe_rate * delta_y * mUp;
	}

// Window size, kept for screenshots
static int gWindowSizeX = 0;
static int gWindowSizeY = 0;

// File locations
std::string vertexShader;
std::string fragmentShader;

float lightPosition[] = {10.0f, 15.0f, 10.0f, 1.0f};

STShaderProgram *shader;
DrawGraph* graph;


// Stored mouse position for camera rotation, panning, and zoom.
int gPreviousMouseX = -1;
int gPreviousMouseY = -1;
int gMouseButton = -1;



void ReshapeCallback(int w, int h)
{
	gWindowSizeX = w;
	gWindowSizeY = h;

	glViewport(0, 0, gWindowSizeX, gWindowSizeY);

	glMatrixMode( GL_PROJECTION );
	glLoadIdentity();
	// Set up a perspective projection
	float aspectRatio = (float) gWindowSizeX / (float) gWindowSizeY;
	gluPerspective(30.0f, aspectRatio, .1f, 10000.0f);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();


	glutPostRedisplay();
}

void SpecialKeyCallback(int key, int x, int y)
{
	switch(key) {
		case GLUT_KEY_LEFT:
			StrafeCamera(10,0);
			break;
		case GLUT_KEY_RIGHT:
			StrafeCamera(-10,0);
			break;
		case GLUT_KEY_DOWN:
			StrafeCamera(0,-10);
			break;
		case GLUT_KEY_UP:
			StrafeCamera(0,10);
			break;
		default:
			break;
	}
	glutPostRedisplay();
}

void KeyCallback(unsigned char key, int x, int y)
{
	// TO DO: Any new key press events must be added to this function

	switch(key) {
	case 's': {
			//
			// Take a screenshot, and save as screenshot.jpg
			//
			STImage* screenshot = new STImage(gWindowSizeX, gWindowSizeY);
			screenshot->Read(0,0);
			screenshot->Save("./scr/snap.jpg");
			delete screenshot;
		}
		break;
	case 'r':
		resetCamera();
		break;
	case 'u':
		resetUp();
		break;
	case 'q':
		exit(0);
	default:
		break;
	}

	glutPostRedisplay();
}

void MouseCallback(int button, int state, int x, int y)
{
	if (button == GLUT_LEFT_BUTTON
		|| button == GLUT_RIGHT_BUTTON
		|| button == GLUT_MIDDLE_BUTTON)
	{
		gMouseButton = button;
	} else
	{
		gMouseButton = -1;
	}

	if (state == GLUT_UP)
	{
		gPreviousMouseX = -1;
		gPreviousMouseY = -1;
	}
}

/**
 * Mouse active motion callback (when button is pressed)
 */
void MouseMotionCallback(int x, int y)
{
	if (gPreviousMouseX >= 0 && gPreviousMouseY >= 0)
	{
		//compute delta
		float deltaX = x-gPreviousMouseX;
		float deltaY = y-gPreviousMouseY;
		gPreviousMouseX = x;
		gPreviousMouseY = y;

		//orbit, strafe, or zoom
		if (gMouseButton == GLUT_LEFT_BUTTON)
		{
			RotateCamera(deltaX, deltaY);
		}
		else if (gMouseButton == GLUT_MIDDLE_BUTTON)
		{
			StrafeCamera(deltaX, deltaY);
		}
		else if (gMouseButton == GLUT_RIGHT_BUTTON)
		{
			ZoomCamera(deltaY);
		}

	} else
	{
		gPreviousMouseX = x;
		gPreviousMouseY = y;
	}


	glutPostRedisplay();

}

void idleFunc(){
	if(graph->getSize())
		glutPostRedisplay();
}


void Setup()
{
	// Set up lighting variables in OpenGL
	// Once we do this, we will be able to access them as built-in
	// attributes in the shader (see examples of this in normalmap.frag)
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	// Light source attributes
	static float ambientLight[]  = {0.40, 0.40, 0.40, 1.0};
	static float diffuseLight[]  = {1.00, 1.00, 1.00, 1.0};
	static float specularLight[] = {1.00, 1.00, 1.00, 1.0};
	glLightfv(GL_LIGHT0, GL_SPECULAR,  specularLight);
	glLightfv(GL_LIGHT0, GL_AMBIENT,   ambientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE,   diffuseLight);

	shader = new STShaderProgram();
	shader->LoadVertexShader(vertexShader);
	shader->LoadFragmentShader(fragmentShader);

	resetCamera();

	glClearColor(0.5f, 0.5f, 0.5f, 1.0f);
	glEnable(GL_DEPTH_TEST);

	graph = new DrawGraph("localhost", "8089");

}

void DisplayCallback()
{
	graph->processQue();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	SetUpAndRight();

	gluLookAt(mPosition.x,mPosition.y,mPosition.z,
			  mLookAt.x,mLookAt.y,mLookAt.z,
			  mUp.x,mUp.y,mUp.z);

	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	// Bind the textures we've loaded into openGl to
	// the variable names we specify in the fragment
	// shader.
	shader->SetTexture("normalTex", 0);
	shader->SetTexture("displacementTex", 1);
	shader->SetTexture("colorTex", 2);

	// Invoke the shader.  Now OpenGL will call our
	// shader programs on anything we draw.
//	shader->Bind();

	graph->Draw(shader);

//	shader->UnBind();

	glutSwapBuffers();
}

//-------------------------------------------------
// main program loop
//-------------------------------------------------
int main(int argc, char** argv)
{
	//---------------------------------------------------------------------------
	// TO DO: Change this file name to change the .obj model that is loaded
	// Optional: read in the file name from the command line > proj1_mesh myfile.obj
	//--------------------------------------------------------------------------
	//~ meshOBJ		= std::string("../../data/meshes/cone.obj");


	vertexShader   = std::string("res/default.vert");
	fragmentShader = std::string("res/phong.frag");

	//
	// Initialize GLUT.
	//
	glutInit(&argc, argv);
	glutInitDisplayMode( GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
	glutInitWindowPosition(20, 20);
	glutInitWindowSize(1024, 768);
	glutCreateWindow("proj1_mesh");

	//
	// Initialize GLEW.
	//
#ifndef __APPLE__
	glewInit();
	if(!GLEW_VERSION_2_0) {
		printf("Your graphics card or graphics driver does\n"
			   "\tnot support OpenGL 2.0, trying ARB extensions\n");

		if(!GLEW_ARB_vertex_shader || !GLEW_ARB_fragment_shader) {
			printf("ARB extensions don't work either.\n");
			printf("\tYou can try updating your graphics drivers.\n"
				   "\tIf that does not work, you will have to find\n");
			printf("\ta machine with a newer graphics card.\n");
			exit(1);
		}
	}
#endif

	// Be sure to initialize GLUT (and GLEW for this assignment) before
	// initializing your application.

	Setup();

	glutDisplayFunc(DisplayCallback);
	glutReshapeFunc(ReshapeCallback);
	glutSpecialFunc(SpecialKeyCallback);
	glutKeyboardFunc(KeyCallback);
	glutMouseFunc(MouseCallback);
	glutMotionFunc(MouseMotionCallback);
	glutIdleFunc(idleFunc);

	glutMainLoop();


	return 0;
}
