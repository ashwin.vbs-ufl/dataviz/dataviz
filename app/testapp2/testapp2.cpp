#include <string>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <iostream>
#include <netdb.h>
#include <unistd.h>
#include <cstdlib>

int sd;
addrinfo* res;
float random(float max){
	float a = ((float)std::rand()/RAND_MAX) * max * 2;
	return a - max;
}

int randInt(int num){
	return std::rand()%num;
}

void initConnection(){
	addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;  // use IPv4 or IPv6, whichever
	hints.ai_socktype = SOCK_DGRAM;
//	hints.ai_flags = AI_PASSIVE;
	getaddrinfo("localhost", "8089", &hints, &res);
	sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
}

std::string getDataPtString(int a, float b, float c){
	std::string comm("newDataPoint(name"), comma(", "), end(")"), sa, sb, sc;
	sa = std::to_string(a);
	sb = std::to_string(b);
	sc = std::to_string(c);
	return (comm + sa + comma + sb + comma + sc + end);
}

std::string getDataSetString(int a, float b){
	std::string comm("setDataSize(name"), comma(", "), end(")"), sa, sb;
	sa = std::to_string(a);
	sb = std::to_string(b);
	return (comm + sa + comma + sb + end);
}

void Send(std::string msg){
	std::cout<<msg<<std::endl;
	sendto(sd, msg.c_str(), msg.length(), 0, res->ai_addr, res->ai_addrlen);
}

int main(){
	initConnection();

	std::string type("setType(sphere)");
	Send(type);
	std::string scale("setScale(0.03)");
	Send(scale);

	for(int i = 0; i <= 10; i++){
		Send(getDataPtString(i, 29.65f + random(0.16), -82.32 + random(0.39)));
	}

	for(int i = 0; i < 100000; i++){
		usleep(100000);
		Send(getDataSetString(randInt(10), randInt(100)));
	}

}
