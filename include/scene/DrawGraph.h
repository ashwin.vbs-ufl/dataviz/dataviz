#pragma once

#include <tree/Graph.h>
#include <stglew.h>
#include <scene/Meshes.h>

class DrawGraph : public Graph {


	STImage   *textureImg;
	STTexture *textureTex;

	Sphere sphere;
	Cylinder cylinder;
	Cube cube;

	void drawWorld(STShaderProgram *shader);
	void drawSphereData(DataPtSphere*, STShaderProgram *shader, float max);
	void drawPointMapData(DataPt3D*, STShaderProgram *shader, float max);

public:
	DrawGraph(std::string ip, std::string port);
	void Draw(STShaderProgram * shader);
};
