#pragma once

class thermal{
private:
	static float r[11], g[11], b[11];
public:
	static void initialize();
	static float getR(float);
	static float getG(float);
	static float getB(float);
};


