#pragma once

#include <vector>
#include <string>
#include "stglew.h"

struct VectorID{
	unsigned int x, y, z;
	VectorID(unsigned int a, unsigned int b, unsigned int c) : x(a), y(b), z(c) {}
	~VectorID(){}
};

class Mesh{
private:

protected:
	STTriangleMesh* m_mesh;
	float m_r, m_g, m_b;
	std::vector<STVector3>	Vertices;
	std::vector<VectorID>	Triangles;
	unsigned int checkAndInsertMidPt(unsigned int vec1, unsigned int vec2);

public:
	Mesh(): m_mesh(nullptr), m_r(0), m_g(0), m_b(0) {}
	virtual ~Mesh() {delete m_mesh;}

	void render(bool);
	void build();
	void calcTexCoord();
	bool write(std::string filename);
	void toggleAxii();
	void setColor(float r, float g, float b);
};

class Sphere : public Mesh {
public:
	Sphere();
};

class Cube: public Mesh{
public:
	Cube();
};

class Cylinder: public Mesh{
public:
	Cylinder();
};
