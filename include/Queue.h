#pragma once

#include <deque>
#include <string>
#include <thread>
#include <mutex>

class Queue{
private:
	std::deque<std::string> m_Q;
	std::mutex m_QMutex;
	bool m_stop;
	std::string address, port;
	std::thread trd;
public:
	Queue(std::string ip, std::string port);
	~Queue();
	size_t getSize(){return m_Q.size();}
	std::string pop();
	static void Thread(Queue*);
};
