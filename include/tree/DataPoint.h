#pragma once

class DataPoint {
public:
	float measurement;
};

class DataPt3D : public DataPoint{
public:
	float x, y, z;
};

class DataPtSphere : public DataPoint{
public:
	float Lat, Long;
};
