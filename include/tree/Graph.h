#pragma once
#include <string>
#include <map>
#include "tree/DataPoint.h"
#include "Queue.h"


class Graph {
public:
	enum class Commands{
		setType,
		setScale,
		newDataPoint,
		remDataPoint,
		setDataSize,
		incDataSize,
		ERROR,
	};
	enum class GraphType{
		sphere,
		pointmap
	};
private:
	Queue* q;
	static std::map<Graph::Commands, std::string> CommandNameMap;
	void setType(std::string);
	float popFloat(std::string&);
	std::string popString(std::string&);
	void handleCommand(std::string);
protected:
	std::map<std::string, DataPoint*> Children;
	float scale;
	GraphType graphType;
public:
	Graph(std::string ip, std::string port);
	void processQue();
	int getSize() {return q->getSize();}
	virtual ~Graph();
	DataPoint* getDataPt(std::string name) {return Children.find(name)->second;}
};


//setType(sphere, thermal) <sphere, pointmap> <sphere -> thermal, bar> <pointmap -> thermal>
//setScale(<num>)
//newDataPoint(name, a, b, c)
//remDataPoint(name)
//setDataSize(name, size)
//incDataSize(name)
