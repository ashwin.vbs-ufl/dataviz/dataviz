.PHONY: all
all: app

.PHONY: test
test: lib
	cd ./test && $(MAKE) all

.PHONY: app
app: lib
	cd ./app && $(MAKE) all
	
.PHONY: lib
lib: ext_lib
	cd ./lib && $(MAKE) all

.PHONY: ext_lib
ext_lib:
	cd ./ext_lib && $(MAKE) all

.PHONY: pristine
pristine: clean
	cd ./ext_lib && $(MAKE) clean

.PHONY: clean
clean:
	cd ./test && $(MAKE) clean
	cd ./app && $(MAKE) clean
	cd ./lib && $(MAKE) clean
