#pragma once
#include "Framework/TestCase.h"
#include "tree/Graph.h"
#include <vector>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <string.h>
#include <netdb.h>

namespace test{
	class NetworkTest : public TestCase{
	private:
		Graph* testGraph;
		std::vector<std::string> msgs;
		int sd;
		addrinfo *res;
	public:
		NetworkTest();
		virtual ~NetworkTest();
		void test();
	};
}
