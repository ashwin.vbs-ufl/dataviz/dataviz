#pragma once

#include<string>

namespace test{
	class TestCase{
	private:
	public:
		void run();
	protected:
		std::string name;
		TestCase(std::string);
		~TestCase();
		virtual void test() = 0;
	};
}
