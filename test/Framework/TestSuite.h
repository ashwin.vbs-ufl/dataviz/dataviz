#pragma once

#include<vector>

#include "StringTable.h"
#include "TestCase.h"

namespace test{
	class TestSuite{
	private:
		std::string name;
		std::vector<TestCase*> m_TestList;
	public:
		TestSuite();
		~TestSuite();
		void run();
	};
}
