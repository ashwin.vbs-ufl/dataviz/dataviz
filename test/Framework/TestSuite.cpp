#include"TestSuite.h"
#include <iostream>
#include "StringTable.h"

using namespace test;

TestSuite::~TestSuite(){

}

void TestSuite::run(){
	std::cout<<StringTable.at(StringEnum::TestSuiteStart)<<std::endl;
	std::cout<<StringTable.at(StringEnum::TestSuiteName)<<name<<std::endl;
	for(auto it = m_TestList.begin(); it != m_TestList.end(); it++)
		(*it)->run();
	std::cout<<StringTable.at(StringEnum::TestSuiteResult)<<std::endl;
	std::cout<<StringTable.at(StringEnum::TestSuiteEnd)<<std::endl;
	std::cout<<std::endl;
}
