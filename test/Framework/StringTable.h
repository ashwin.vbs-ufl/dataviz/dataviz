#pragma once

#include <map>

namespace test{
	enum class StringEnum{
		TestSuiteStart,
		TestSuiteName,
		TestSuiteResult,
		TestSuiteEnd,
		TestCaseStart,
		TestCaseName,
		TestCaseResult,
		TestCaseException,
		TestCaseEnd,
	};

	extern const std::map<StringEnum, std::string> StringTable;
}
