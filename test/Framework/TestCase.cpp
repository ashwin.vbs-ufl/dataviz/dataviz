#include"TestCase.h"
#include <iostream>
#include "StringTable.h"

using namespace test;

TestCase::TestCase(std::string nm) : name(nm){

}

TestCase::~TestCase(){

}

void TestCase::run(){
	std::cout<<StringTable.at(StringEnum::TestCaseStart)<<std::endl;
	std::cout<<StringTable.at(StringEnum::TestCaseName)<<name<<std::endl;
	try{
		test();
		std::cout<<StringTable.at(StringEnum::TestCaseResult)<<"Pass"<<std::endl;
	}
	catch(std::exception& error){
		std::cout<<StringTable.at(StringEnum::TestCaseResult)<<"Fail"<<std::endl;
		std::cout<<StringTable.at(StringEnum::TestCaseException)<<error.what()<<std::endl;
	}
	std::cout<<StringTable.at(StringEnum::TestCaseEnd)<<std::endl;
//	std::cout<<std::endl;
}
