#include "StringTable.h"

namespace test{
	const std::map<StringEnum, std::string> StringTable = {
		{StringEnum::TestSuiteStart, "***"},
		{StringEnum::TestSuiteName, "Test Suite Start: "},
		{StringEnum::TestSuiteResult, "Test Suite End: "},
		{StringEnum::TestSuiteEnd, "***"},
		{StringEnum::TestCaseStart, "\t***"},
		{StringEnum::TestCaseName, "\tTest Case: "},
		{StringEnum::TestCaseResult, "\t\tResult: "},
		{StringEnum::TestCaseException, "\t\tException: "},
		{StringEnum::TestCaseEnd, "\t***"}
	};
}
