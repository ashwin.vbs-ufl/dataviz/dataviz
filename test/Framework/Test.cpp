#include <iostream>

#include "StringTable.h"
#include "TestSuite.h"

using namespace test;

int main(){
	TestSuite suite;
	suite.run();
	return 0;
}
