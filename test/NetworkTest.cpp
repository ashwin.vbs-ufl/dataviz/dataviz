#include "NetworkTest.h"
#include <unistd.h>

using namespace test;

NetworkTest::NetworkTest() : TestCase("NetworkTest"){
	testGraph = new Graph("localhost", "8089");
	msgs.insert(msgs.end(), "setType(sphere)");
	msgs.insert(msgs.end(), "setScale(4)");
	msgs.insert(msgs.end(), "newDataPoint(ny, 3, 4)");
	msgs.insert(msgs.end(), "newDataPoint(ny2, 34, 42)");
	msgs.insert(msgs.end(), "remDataPoint(ny2)");
	msgs.insert(msgs.end(), "setDataSize(ny, 4)");
	msgs.insert(msgs.end(), "incDataSize(ny)");
	//msgs.insert(msgs.end(), "setType(pointmap)");


	addrinfo hints;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;  // use IPv4 or IPv6, whichever
	hints.ai_socktype = SOCK_DGRAM;
//	hints.ai_flags = AI_PASSIVE;
	getaddrinfo("localhost", "8089", &hints, &res);
	sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
}

NetworkTest::~NetworkTest(){
	delete testGraph;
}

void NetworkTest::test(){
	for(auto it = msgs.begin(); it != msgs.end(); it++){
		sendto(sd, it->c_str(), it->length(), 0, res->ai_addr, res->ai_addrlen);
		sleep(1);
		testGraph->processQue();
	}
	DataPtSphere *test = (DataPtSphere*)testGraph->getDataPt("ny");
	if(test->measurement != 5)
		throw "measurement not correct";
	if(test->Lat != 3 || test->Long != 4)
		throw "latlong not correct";
}
