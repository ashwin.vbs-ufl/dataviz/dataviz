#Usage:
#Each make file should include PATH_TO_ROOT variable.
#LIBS and INCPATHS should be relative to ROOT eg. /lib/system
#For lib, define INCPATHS, SRCS and LIB
#For app, define INCPATHS, SRCS, LIBS, LIBFLAGS and APP
#For test, define INCPATHS, TEST_TEMPLATE, LIBS and LIBFLAGS

#paths
CURRENT_DIR=$(shell pwd)
ROOT=$(CURRENT_DIR)/$(PATH_TO_ROOT)/

#build configuration setting
ifndef BC
BC = debug
endif

#build rules for C++ files
CC = g++
CPPFLAGS += -Wall -Werror -std=c++11 -fdiagnostics-color=auto
DEFINES += _X11

AR = ar
RANLIB = ranlib
ARFLAGS = r

ifeq ($(BC),debug)
CPPFLAGS += -g3
else
CPPFLAGS += -O2
endif

#srcs =
OBJS = $(SRCS:.cpp=.o)
DEPS = $(SRCS:.cpp=.d)

-include $(DEPS)

INCFLAGS = $(addprefix -I, $(INCPATHS))
DEFINEFLAGS = $(addprefix -D, $(DEFINES))
%.o: %.cpp
	$(CC) -c $(CPPFLAGS) $(DEFINEFLAGS) $(INCFLAGS) $*.cpp -o $*.o
	@$(CC) -MM -MG -MP -MF $*.d -MT '$*.o' $(CPPFLAGS) $(INCFLAGS) $*.cpp

$(LIB): $(OBJS)
	$(AR) $(ARFLAGS) $@ $?
	$(RANLIB) $(LIB)

MLIBFLAGS = $(LIBS) $(LIBFLAGS)
$(APP): $(OBJS) $(LIBS)
	$(CC) -o $@ $(OBJS) $(MLIBFLAGS)

#%.test: %.cpp
#	$(CC) -c $(CPPFLAGS) $(INCFLAGS) $(DEFINEFLAGS) $*.cpp -o $*.o
#	$(CC) -o $*.test $(TEST_TEMPLATE) $*.o $(MLIBFLAGS)
#	@$(CC) -MM -MG -MP -MF $*.d -MT '$*.o' $(CPPFLAGS) $(INCFLAGS) $*.cpp

clean:
	rm -f $(OBJS) $(DEPS) $(LIB) $(APP) #$(TESTS)
