#include "tree/Graph.h"
#include <algorithm>

std::map<Graph::Commands, std::string> Graph::CommandNameMap;

Graph::Graph(std::string ip, std::string port) : q(new Queue(ip, port)){
	Graph::CommandNameMap.insert(std::make_pair(Commands::setType, "setType"));
	Graph::CommandNameMap.insert(std::make_pair(Commands::setScale, "setScale"));
	Graph::CommandNameMap.insert(std::make_pair(Commands::newDataPoint, "newDataPoint"));
	Graph::CommandNameMap.insert(std::make_pair(Commands::remDataPoint, "remDataPoint"));
	Graph::CommandNameMap.insert(std::make_pair(Commands::setDataSize, "setDataSize"));
	Graph::CommandNameMap.insert(std::make_pair(Commands::incDataSize, "incDataSize"));
	scale = 1;
	graphType = GraphType::sphere;
}

Graph::~Graph(){
	delete q;
}

float Graph::popFloat(std::string& input){
	return std::stof(popString(input));
}

std::string Graph::popString(std::string& input){
	auto output = input.substr(0, input.find(','));
	input = input.substr(1 + input.find(','));
	return std::move(output);
}

void Graph::setType(std::string tp){
	for(auto it = Children.begin(); it != Children.end(); it++){
		delete it->second;
		Children.erase(it);
	}
	if(tp == "sphere")
		graphType = GraphType::sphere;
	else if (tp == "pointmap")
		graphType = GraphType::pointmap;
	else
		throw "unknown graph type";
}

void Graph::processQue(){
	for(int i = 0; i<=1000 && q->getSize(); i++)
		handleCommand(q->pop());
}

void Graph::handleCommand(std::string input){
	input.erase(std::remove_if(input.begin(), input.end(), isspace), input.end());
	size_t br1 = input.find('(');
	size_t br2 = input.find(')');
	if(br1 == std::string::npos || br2 == std::string::npos)
		throw("syntax issue, not found brackets");
	std::string command = input.substr(0, br1);
	std::string arguments = input.substr(br1 + 1, br2 - br1 - 1);
	Commands enumCommand = Commands::ERROR;
	for(auto it = Graph::CommandNameMap.begin(); it != Graph::CommandNameMap.end(); it++)
		if(it->second == command)
			enumCommand = it->first;
	switch(enumCommand){
	case Commands::setType:
		setType(arguments);
		break;
	case Commands::setScale:
		scale = std::stof(arguments);
		break;
	case Commands::newDataPoint: //TODO: implement
		switch(graphType){
		case GraphType::sphere:{
				auto pt1 = new DataPtSphere();
				auto name = popString(arguments);
				pt1->Lat = popFloat(arguments);
				pt1->Long = popFloat(arguments);
				Children.insert(std::make_pair(name, pt1));
			}
			break;
		case GraphType::pointmap:{
				auto pt2 = new DataPt3D();
				auto name = popString(arguments);
				pt2->x = popFloat(arguments);
				pt2->y = popFloat(arguments);
				pt2->z = popFloat(arguments);
				Children.insert(std::make_pair(name, pt2));
			}
			break;
		default:
			break;
		}
		break;
	case Commands::remDataPoint:
		Children.erase(Children.find(arguments));
		break;
	case Commands::setDataSize: {
			auto it = Children.find(popString(arguments));
			if (it != Children.end())
				it->second->measurement = popFloat(arguments);
			else
				throw "data not found";
		}
		break;
	case Commands::incDataSize: {
			auto it = Children.find(arguments);
			if (it != Children.end())
				it->second->measurement += 1;
			else
				throw "data not found";
		}
		break;
	case Commands::ERROR:
	default:
		throw "unrecognized command";
		break;
	}
}
