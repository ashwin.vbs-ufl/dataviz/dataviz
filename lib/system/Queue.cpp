#include "Queue.h"
#include <utility>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <netdb.h>
#include <string.h>
#include <vector>

Queue::Queue(std::string ip, std::string prt): m_stop(false), address(ip), port(prt){
//	m_Q.push_back("newDataPoint(pt2, 12.6, 77.34)");
//	m_Q.push_back("setDataSize(pt2, 1)");
//	m_Q.push_back("newDataPoint(pt1, 29.39, -82.19)");
//	m_Q.push_back("setDataSize(pt1, 2)");
//	m_Q.push_back("setScale(0.2)");

//	m_Q.push_back("setType(pointmap)");
//	m_Q.push_back("newDataPoint(pt2, 5, 5, 5)");
//	m_Q.push_back("setDataSize(pt2, 1)");
//	m_Q.push_back("newDataPoint(pt1, 5, 4, 2)");
//	m_Q.push_back("setDataSize(pt1, 2)");
	trd = std::thread(&Queue::Thread, this);
}

Queue::~Queue(){
	m_stop = true;
	trd.join();
}


std::string Queue::pop(){
	if(!getSize()) return "";
	std::string temp = m_Q.front();
	std::unique_lock<std::mutex> lock(m_QMutex);
	m_Q.pop_front();
	return std::move(temp);
}

void Queue::Thread(Queue* ref){
	std::vector<unsigned char> buffer;
	fd_set readfds;
	struct addrinfo hints, *res;
	int sd;
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;  // use IPv4 or IPv6, whichever
	hints.ai_socktype = SOCK_DGRAM;
//	hints.ai_flags = AI_PASSIVE;
	getaddrinfo(ref->address.c_str(), ref->port.c_str(), &hints, &res);
	sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	bind(sd, res->ai_addr, res->ai_addrlen);

	while(!ref->m_stop){
		FD_ZERO(&readfds);
		FD_SET(sd, &readfds);
		timeval delay;
		delay.tv_sec = 1; // half a second
	    if(select(sd + 1, &readfds, nullptr, nullptr, &delay) == -1)
	    	throw "select failed";
	    if(FD_ISSET(sd, &readfds)){
	    	int size;
	    	if(ioctl(sd, FIONREAD, &size) != -1){
	    		buffer.resize(size);
//				unsigned int size = sizeof(*addr);
	    		if(::recvfrom(sd, buffer.data(), buffer.size(), 0, nullptr, 0) == -1)
	    			throw "recv failed";
	    		std::unique_lock<std::mutex> lock(ref->m_QMutex);
	    		ref->m_Q.push_back(std::move(std::string(buffer.begin(), buffer.end())));
	    		buffer.clear();
	    	}
	    }
	}
}
