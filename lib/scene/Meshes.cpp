#include "scene/Meshes.h"
#include <iostream>

void Mesh::render(bool type){
	m_mesh->Draw(type);
}

bool Mesh::write(std::string filename){
//	if(!filename.length())
//		return m_mesh->Write("..\..\data\meshes\mysphere.obj");
//	return m_mesh->Write(filename);
	return true;
}

void Mesh::setColor(float r, float g, float b){
	m_r = r;
	m_g = g;
	m_b = b;


	m_mesh->mMaterialAmbient[0]=m_r*0.85;
	m_mesh->mMaterialAmbient[1]=m_g*0.85;
	m_mesh->mMaterialAmbient[2]=m_b*0.85;

	m_mesh->mMaterialDiffuse[0]=m_r;
	m_mesh->mMaterialDiffuse[1]=m_g;
	m_mesh->mMaterialDiffuse[2]=m_b;

	m_mesh->mMaterialSpecular[0]=1.0f;
	m_mesh->mMaterialSpecular[1]=1.0f;
	m_mesh->mMaterialSpecular[2]=1.0f;


	calcTexCoord();
}

void Mesh::build(){

	delete m_mesh;
	m_mesh = new STTriangleMesh();
	m_mesh->mDrawAxis = false;

	for (std::vector<STVector3>::iterator it = Vertices.begin(); it != Vertices.end(); it++)
		m_mesh->AddVertex(it->x, it->y, it->z);
	for(std::vector<VectorID>::iterator it = Triangles.begin(); it != Triangles.end(); it++)
		m_mesh->AddFace(it->x, it->y, it->z);

	m_mesh->Build();

	m_mesh->mMaterialAmbient[0]=m_r*0.85;
	m_mesh->mMaterialAmbient[1]=m_g*0.85;
	m_mesh->mMaterialAmbient[2]=m_b*0.85;

	m_mesh->mMaterialDiffuse[0]=m_r;
	m_mesh->mMaterialDiffuse[1]=m_g;
	m_mesh->mMaterialDiffuse[2]=m_b;

	m_mesh->mMaterialSpecular[0]=1.0f;
	m_mesh->mMaterialSpecular[1]=1.0f;
	m_mesh->mMaterialSpecular[2]=1.0f;

	m_mesh->mShininess=8.0f;

	calcTexCoord();
}

void Mesh::calcTexCoord(){
	//_mesh->Build();
	m_mesh->CalculateTextureCoordinatesViaSphericalProxy();
}

void Mesh::toggleAxii(){
	m_mesh->mDrawAxis = !m_mesh->mDrawAxis;
}

Sphere::Sphere(){

	//init vertices


	m_r = 0.8;
	m_g = 0.8;
	m_b = 0.8;


	Vertices.push_back(STVector3(1, 0, 0));
	Vertices.push_back(STVector3(0, 1, 0));
	Vertices.push_back(STVector3(-1, 0, 0));
	Vertices.push_back(STVector3(0, -1, 0));
	Vertices.push_back(STVector3(0, 0, 1));
	Vertices.push_back(STVector3(0, 0, -1));

	Triangles.push_back(VectorID(0,1,4));
	Triangles.push_back(VectorID(1,2,4));
	Triangles.push_back(VectorID(2,3,4));
	Triangles.push_back(VectorID(3,0,4));
	Triangles.push_back(VectorID(1,0,5));
	Triangles.push_back(VectorID(2,1,5));
	Triangles.push_back(VectorID(3,2,5));
	Triangles.push_back(VectorID(0,3,5));

	build();
	m_mesh->LoopSubdivide();
	m_mesh->LoopSubdivide();
	m_mesh->LoopSubdivide();
	m_mesh->LoopSubdivide();
	m_mesh->LoopSubdivide();
	m_mesh->LoopSubdivide();
	m_mesh->LoopSubdivide();
	for(auto it = m_mesh->mVertices.begin(); it != m_mesh->mVertices.end(); it++){
		auto mag = sqrt((double)(((*it)->pt.x)*((*it)->pt.x)) + (((*it)->pt.y)*((*it)->pt.y)) + (((*it)->pt.z)*((*it)->pt.z)));
		(*it)->pt.x = (*it)->pt.x/mag;
		(*it)->pt.y = (*it)->pt.y/mag;
		(*it)->pt.z = (*it)->pt.z/mag;
	}

	calcTexCoord();
}


Cube::Cube(){
	Vertices.push_back(STVector3(0.5, -0.5, 0));
	Vertices.push_back(STVector3(0.5, -0.5, 1.0));
	Vertices.push_back(STVector3(-0.5, -0.5, 1.0));
	Vertices.push_back(STVector3(-0.5, -0.5, 0));
	Vertices.push_back(STVector3(0.5, 0.5, 0));
	Vertices.push_back(STVector3(0.5, 0.5, 1.0));
	Vertices.push_back(STVector3(-0.5, 0.5, 1.0));
	Vertices.push_back(STVector3(-0.5, 0.5, 0));


	Triangles.push_back(VectorID(0, 1, 2));
	Triangles.push_back(VectorID(0, 2, 3));
	Triangles.push_back(VectorID(4, 7, 6));
	Triangles.push_back(VectorID(4, 6, 5));
	Triangles.push_back(VectorID(0, 4, 5));
	Triangles.push_back(VectorID(0, 5, 1));
	Triangles.push_back(VectorID(1, 5, 6));
	Triangles.push_back(VectorID(1, 6, 2));
	Triangles.push_back(VectorID(2, 6, 7));
	Triangles.push_back(VectorID(2, 7, 3));
	Triangles.push_back(VectorID(4, 0, 3));
	Triangles.push_back(VectorID(4, 3, 7));

	build();
}

Cylinder::Cylinder(){
//	Vertices.push_back(STVector3(1, 0, 0));
//	Vertices.push_back(STVector3(0.7071, -0.7071, 0));
//	Vertices.push_back(STVector3(0, -1, 0));
//	Vertices.push_back(STVector3(-0.7071, -0.7071, 0));
//	Vertices.push_back(STVector3(-1, 0, 0));
//	Vertices.push_back(STVector3(-0.7071, 0.7071, 0));
//	Vertices.push_back(STVector3(0, 1, 0));
//	Vertices.push_back(STVector3(0.7071, 0.7071, 0));
//	Vertices.push_back(STVector3(1, 0, 1));
//	Vertices.push_back(STVector3(0.7071, -0.7071, 1));
//	Vertices.push_back(STVector3(0, -1, 1));
//	Vertices.push_back(STVector3(-0.7071, -0.7071, 1));
//	Vertices.push_back(STVector3(-1, 0, 1));
//	Vertices.push_back(STVector3(-0.7071, 0.7071, 1));
//	Vertices.push_back(STVector3(0, 1, 1));
//	Vertices.push_back(STVector3(0.7071, 0.7071, 1));
//	Vertices.push_back(STVector3(0, 0, 0));
//	Vertices.push_back(STVector3(0, 0, 1));

	Vertices.push_back(STVector3(1, 0, 0));
	Vertices.push_back(STVector3(0.7071, 0, 0.7071));
	Vertices.push_back(STVector3(0, 0, 1));
	Vertices.push_back(STVector3(-0.7071, 0, 0.7071));
	Vertices.push_back(STVector3(-1, 0, 0));
	Vertices.push_back(STVector3(-0.7071, 0, -0.7071));
	Vertices.push_back(STVector3(0, 0, -1));
	Vertices.push_back(STVector3(0.7071, 0, -0.7071));
	Vertices.push_back(STVector3(1, 1, 0));
	Vertices.push_back(STVector3(0.7071, 1, 0.7071));
	Vertices.push_back(STVector3(0, 1, 1));
	Vertices.push_back(STVector3(-0.7071, 1, 0.7071));
	Vertices.push_back(STVector3(-1, 1, 0));
	Vertices.push_back(STVector3(-0.7071, 1, -0.7071));
	Vertices.push_back(STVector3(0, 1, -1));
	Vertices.push_back(STVector3(0.7071, 1, -0.7071));
	Vertices.push_back(STVector3(0, 0, 0));
	Vertices.push_back(STVector3(0, 1, 0));



	Triangles.push_back(VectorID(0, 8, 1));
	Triangles.push_back(VectorID(1, 9, 2));
	Triangles.push_back(VectorID(2, 10, 3));
	Triangles.push_back(VectorID(3, 11, 4));
	Triangles.push_back(VectorID(4, 12, 5));
	Triangles.push_back(VectorID(5, 13, 6));
	Triangles.push_back(VectorID(6, 14, 7));
	Triangles.push_back(VectorID(7, 15, 0));

	Triangles.push_back(VectorID(1, 8, 9));
	Triangles.push_back(VectorID(2, 9, 10));
	Triangles.push_back(VectorID(3, 10, 11));
	Triangles.push_back(VectorID(4, 11, 12));
	Triangles.push_back(VectorID(5, 12, 13));
	Triangles.push_back(VectorID(6, 13, 14));
	Triangles.push_back(VectorID(7, 14, 15));
	Triangles.push_back(VectorID(0, 15, 8));

	Triangles.push_back(VectorID(0, 1, 16));
	Triangles.push_back(VectorID(1, 2, 16));
	Triangles.push_back(VectorID(2, 3, 16));
	Triangles.push_back(VectorID(3, 4, 16));
	Triangles.push_back(VectorID(4, 5, 16));
	Triangles.push_back(VectorID(5, 6, 16));
	Triangles.push_back(VectorID(6, 7, 16));
	Triangles.push_back(VectorID(7, 0, 16));

	Triangles.push_back(VectorID(9, 8, 17));
	Triangles.push_back(VectorID(10, 9, 17));
	Triangles.push_back(VectorID(11, 10, 17));
	Triangles.push_back(VectorID(12, 11, 17));
	Triangles.push_back(VectorID(13, 12, 17));
	Triangles.push_back(VectorID(14, 13, 17));
	Triangles.push_back(VectorID(15, 14, 17));
	Triangles.push_back(VectorID(8, 15, 17));

	build();
}
