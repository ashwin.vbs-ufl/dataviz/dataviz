#include "scene/DrawGraph.h"
#include "scene/thermal.h"
#include <cmath>
#define PI 3.14159265

DrawGraph::DrawGraph(std::string ip, std::string port) : Graph(ip, port){
	textureImg = new STImage("./res/clr.jpg");
	textureTex = new STTexture(textureImg);
	textureTex->SetWrap(GL_REPEAT, GL_REPEAT);
	thermal::initialize();
}

void DrawGraph::Draw(STShaderProgram *shader){
	float max = 0.00001;
	for(auto it = Children.begin(); it != Children.end(); it++)
		max = (max > it->second->measurement) ? max : it->second->measurement;
	switch(graphType){
	case GraphType::sphere:{
			drawWorld(shader);
			for(auto it = Children.begin(); it != Children.end(); it++)
				drawSphereData((DataPtSphere*)it->second, shader, max);
		}
		break;
	case GraphType::pointmap:{
			for(auto it = Children.begin(); it != Children.end(); it++)
				drawPointMapData((DataPt3D*)it->second, shader, max);
		}
		break;
	default:
		break;
	}
}

//void DrawGraph::

void DrawGraph::drawWorld(STShaderProgram *shader){
	// Texture 0: surface normal map
	glActiveTexture(GL_TEXTURE0);
	textureTex->Bind();

	shader->SetTexture("colorTex", 0);

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glRotatef(-90,1,0, 0);
	sphere.setColor(1, 1, 1);

	shader->Bind();
	shader->SetUniform("colorMapping", 1.0);
	sphere.render(1);
	shader->UnBind();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glActiveTexture(GL_TEXTURE0);
	textureTex->UnBind();
}

void DrawGraph::drawSphereData(DataPtSphere* pt, STShaderProgram *shader, float max){
	//transformation
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glRotatef(90 - pt->Lat, -1*std::sin(pt->Long*PI/180), 0, -1*std::cos(pt->Long*PI/180));
	glScalef(0.01*scale, 1.0+((pt->measurement/max)*scale), 0.01*scale);
	cylinder.setColor(thermal::getR(pt->measurement/max), thermal::getG(pt->measurement/max), thermal::getB(pt->measurement/max));

	shader->Bind();
	shader->SetUniform("colorMapping", -1.0);
	cylinder.render(0);
	shader->UnBind();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}

void DrawGraph::drawPointMapData(DataPt3D* pt, STShaderProgram *shader, float max){
	//apply transformation
	shader->SetUniform("colorMapping", -1.0);
	cube.setColor(thermal::getR(pt->measurement/max), thermal::getG(pt->measurement/max), thermal::getB(pt->measurement/max));

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(pt->x, pt->y, pt->z);
	glScalef(scale, scale, scale);
	shader->Bind();
	shader->SetUniform("colorMapping", -1.0);
	cube.render(1);
	shader->UnBind();
	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();
}
