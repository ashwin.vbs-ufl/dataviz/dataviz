#include "scene/thermal.h"

float thermal::r[11], thermal::g[11], thermal::b[11];

void thermal::initialize(){
	thermal::r[10] = 165;
	thermal::r[9] = 215;
	thermal::r[8] = 244;
	thermal::r[7] = 253;
	thermal::r[6] = 254;
	thermal::r[5] = 255;
	thermal::r[4] = 224;
	thermal::r[3] = 171;
	thermal::r[2] = 116;
	thermal::r[1] = 69;
	thermal::r[0] = 49;
	thermal::g[10] = 0;
	thermal::g[9] = 48;
	thermal::g[8] = 109;
	thermal::g[7] = 174;
	thermal::g[6] = 224;
	thermal::g[5] = 255;
	thermal::g[4] = 243;
	thermal::g[3] = 217;
	thermal::g[2] = 173;
	thermal::g[1] = 117;
	thermal::g[0] = 54;
	thermal::b[10] = 38;
	thermal::b[9] = 39;
	thermal::b[8] = 67;
	thermal::b[7] = 97;
	thermal::b[6] = 144;
	thermal::b[5] = 191;
	thermal::b[4] = 248;
	thermal::b[3] = 233;
	thermal::b[2] = 209;
	thermal::b[1] = 180;
	thermal::b[0] = 149;
}

float thermal::getR(float in){
	int base = (int)(in*10);
	if(in == 1.0f)
		return r[10]/256;
	else
		return (((1-(in*10)+base)*r[base])+(((in*10)-base)*r[base+1]))/256;
}
float thermal::getG(float in){
	int base = (int)(in*10);
	if(in == 1.0f)
		return g[10]/256;
	else
		return (((1-(in*10)+base)*g[base])+(((in*10)-base)*g[base+1]))/256;
}
float thermal::getB(float in){
	int base = (int)(in*10);
	if(in == 1.0f)
		return b[10]/256;
	else
		return (((1-(in*10)+base)*b[base])+(((in*10)-base)*b[base+1]))/256;;
}
